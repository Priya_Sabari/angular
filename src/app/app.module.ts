import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DisplayStudentsComponent } from './students/display-students/display-students.component';
import { ListStudentsComponent } from './students/list-students/list-students.component';
import { TextTransformPipe } from './text-transform.pipe';
import { TestDirective } from './test.directive';

@NgModule({
  declarations: [
    AppComponent,
    DisplayStudentsComponent,
    ListStudentsComponent,
    TextTransformPipe,
    TestDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
