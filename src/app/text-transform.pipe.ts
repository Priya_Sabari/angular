import { Pipe, PipeTransform } from '@angular/core';
//import { reverse } from 'dns';

@Pipe({
  name: 'textTransform'
})
export class TextTransformPipe implements PipeTransform {

  transform(value: string): string {
    let reversed='';
     for(let char of value){
       reversed=char+reversed;
     };
     return reversed;
  }

}
