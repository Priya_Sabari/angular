import { Directive,Input,ElementRef,HostListener, Host } from '@angular/core';

@Directive({
  selector: '[myHighlight]'
})
export class TestDirective {

  @Input() color:string;
  constructor(public element:ElementRef) { 
   
  //element.nativeElement.style.backgroundColor='rgb(223, 241, 121)';

}

@HostListener('mouseenter') onMouseEnter(){
  this.highlight('#0000FF');
}

@HostListener('mouseleave') onMouseLeave(){
  this.highlight('#FFA500');
}

private highlight(color:string){
  this.element.nativeElement.style.backgroundColor=color;
}
}