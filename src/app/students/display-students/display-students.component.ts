import { Component, OnInit, Input,Output } from '@angular/core';
import { EventEmitter } from 'events';
import { hostViewClassName } from '@angular/compiler';


@Component({
  selector: 'app-display-students',
  templateUrl: './display-students.component.html',

  styleUrls:['./display-students.component.scss']
})
export class DisplayStudentsComponent implements OnInit {

  @Input() public studentObj:any;

  @Output() selectedStudent =new EventEmitter();

  constructor() { }

  ngOnInit(): void {
    console.log(this.studentObj);
  }

 

}
