import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';

import { hostViewClassName } from '@angular/compiler';


@Component({
  selector: 'app-list-students',
  templateUrl: './list-students.component.html',

  styleUrls:['./list-students.component.scss']
})

export class ListStudentsComponent  implements OnInit {
 
  students:Array<any>=[
    {
    id:1 , 
    firstName:'Anu',
     lastName:'Sri',
      studentId:101,
      address:'Chennai',
      mobile:9876543210,
      emailId:'anu@gmail.com' 
    },
    {
    id:2 ,
    firstName:'Siva', 
    lastName:'Prasad', 
    studentId:102,
    address:'Cochin',
    mobile:1234567890,
    emailId:'siva@gmail.com'
  },
    {id:3 ,
     firstName:'Hari',
      lastName:'Haran',
       studentId:103,
       address:'Bangalore',
       mobile:7765432190,
       emailId:'hari@gmail.com' 
      },
    {
      id:4 , 
    firstName:'Nivetha',
     lastName:'Sri',
      studentId:104,
      address:'Hyderabad',
      mobile:8890765432,
      emailId:'nivetha@gmail.com'
     }
    
   
  ];
  selectStudentData:any=null;
 
  constructor() { }

  ngOnInit():void {

  }
  // public highlightRow(student) {
  //   this.selectedFirstName = student.firstName;
  // }
  onSelect=(student:any)=>{
    console.log(student);
    this.selectStudentData=student;
  
  }
clear= ()=> {
this.selectStudentData=false;

}

}